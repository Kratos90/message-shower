const { host, port, keys: storageKeys } = require('./config/redis');

const Redis = require('ioredis');
var redis = new Redis({ port, host });

/** Private methods */

/**
 * @param {Number} time 
 * @returns {Date}
 */
function initDate(time) {
    const date = new Date(time * 1);
    
    date.setSeconds(0);
    date.setMilliseconds(0);
    
    return date;
}

/**
 * @param {Number} timestampForSpecificMinute 
 * @param {String} message 
 */
function putMessageToMinutesTimestampStorage(timestampForSpecificMinute, message) {
    redis.rpush(`${timestampForSpecificMinute}:${storageKeys.messageStorage}`, message);
}

/**
 * @param {Date} date 
 * @param {Number} timestampForSpecificMinute 
 */
function putMinutesTimestampToListOfDailyAvailableMessages(date, timestampForSpecificMinute) {
    const hoursOffsetForDifferentTimeRegions = date.getTimezoneOffset()/60 * -1

    date.setHours(hoursOffsetForDifferentTimeRegions, 0, 0, 0);
    redis.sadd(`${date.getTime()}:${storageKeys.messagesInSpecificDay}`, timestampForSpecificMinute);
}

class MessageSaver {
    /**
     * @param {Number} time 
     */
    constructor(time) {
        this.time = time;
    }

    /**
     * @param {String} message 
     */
    saveMessage(message) {
        const date = initDate(this.time);

        const timestampForSpecificMinute = date.getTime();
        putMessageToMinutesTimestampStorage(timestampForSpecificMinute, message);
        putMinutesTimestampToListOfDailyAvailableMessages(date, timestampForSpecificMinute);
    }
}

module.exports = MessageSaver;