const MessageShower = require('./MessageShower');
const ShowOldMessages = require('./ShowOldMessages');

function checkOldMessages() {
  new ShowOldMessages().showMessagesForPreviousDays();
}

function startIntervalForShowingMessages() {
  setInterval(function() {
    new MessageShower().showMessagesTime();
  }, 20 * 1000);
}

checkOldMessages();
startIntervalForShowingMessages();
