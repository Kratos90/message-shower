const { host, port, keys: storageKeys } = require('./config/redis');

var Redis = require('ioredis');
var redis = new Redis({ port, host });

const MessageShower = require('./MessageShower');

const LIMIT_DAYS_FOR_CHECKING = 30;
const HOURS_IN_DAY = 24;

class ShowOldMessages {
    showMessagesForPreviousDays() {
        const date = new Date();
        this.convertTimeToMidnight(date);
        date.setHours(date.getHours() - HOURS_IN_DAY * LIMIT_DAYS_FOR_CHECKING);
        let i = LIMIT_DAYS_FOR_CHECKING;

        while(i--) {
            this.checkIfMessagesExistsForDay(date);
            date.setHours(date.getHours() + HOURS_IN_DAY);
        }
    }

    /**
     * @param {Date} date 
     */
    convertTimeToMidnight(date) {
        date.setHours(date.getTimezoneOffset()/60 * -1, 0, 0, 0);
    }

    /**
     * @param {Date} date 
     */
    async checkIfMessagesExistsForDay(date) {
        const messagesTimestamps = await redis.smembers(`${date.getTime()}:${storageKeys.messagesInSpecificDay}`);
        for (const timeStamp of messagesTimestamps) {
            new MessageShower(timeStamp).showMessagesTime();
        }
    }
}

module.exports = ShowOldMessages;