# Project Name

Message Shower

## Installation

Please run 'npm i'

## Usage

For starting application you have to run 'npm start' command

You can use following url as GET request for sending new message: "/echoAtTime?time=1563737040000&message=showme"
Also you can use POST request "/echoAtTime" with body which contains the same arguments
Arguments:
> time - argument should be timestamp
> message - string of message

