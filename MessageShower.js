const { host, port, keys: storageKeys } = require('./config/redis');

var Redis = require('ioredis');
var redis = new Redis({ port, host });

class MessageShower {
    /**
     * @param {Number} time 
     */
    constructor(time = 0) {
        if (time) {
            this.currentTime = new Date(time * 1);
        } else {
            this.currentTime = new Date();
            this.setSecondsAndMillisecondsToZero();
        }
    }

    setSecondsAndMillisecondsToZero() {
      this.currentTime.setMilliseconds(0);
      this.currentTime.setSeconds(0);
    }

    getMidnightTimestampForCurrentTime() {
      this.currentTime.setHours(this.currentTime.getTimezoneOffset()/60 * -1, 0, 0, 0);
      return this.currentTime.getTime();
    }

    async showMessagesTime() {
      const timestampForCurrentMinute = this.currentTime.getTime();
      let countOfMessages = await redis.llen(`${timestampForCurrentMinute}:${storageKeys.messageStorage}`);
      let promises = [];
      while(countOfMessages--) {
        promises.push(redis.lpop(`${timestampForCurrentMinute}:${storageKeys.messageStorage}`));
      }

      Promise.all(promises).then(result => {
        for (const message of result) {
          console.log(message);
        }
        redis.srem(`${this.getMidnightTimestampForCurrentTime()}:${storageKeys.messagesInSpecificDay}`, timestampForCurrentMinute)
      });
    }
}

module.exports = MessageShower;