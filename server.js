const http = require('http');
const url = require('url');
const { parse } = require('querystring');

const MessageSaver = require('./MessageSaver');
const { host, port, allowedPaths } = require('./config/server');

const server = http.createServer((req, res) => {
	const { search, pathname} = url.parse(req.url);
	
	if (req.method === 'POST' && allowedPaths.includes(pathname)) {
		var body = '';

        req.on('data', chunk => {
	        body += chunk.toString();
	    });

        req.on('end', function () {
            var { message, time} = parse(body);
			const currentDate = new Date();
			
            if (time && message && currentDate.getTime() < time * 1) {
				new MessageSaver(time).saveMessage(message);
            }

		  	res.statusCode = 200;
		  	res.end();
        });
	} else if (req.method === 'GET' && allowedPaths.includes(pathname)) {
		const currentDate = new Date();
		const searchParams = new URLSearchParams(search);

		if (searchParams.has('time') && searchParams.has('message') && currentDate.getTime() < searchParams.get('time') * 1) {
			new MessageSaver(searchParams.get('time')).saveMessage(searchParams.get('message'));
		}
		
		res.statusCode = 200;
		res.end();
	} else {
		res.statusCode = 404;
		res.end();
	}
});

server.listen(port, host, () => {
  	console.log(`Server running at http://${host}:${port}/`);
});

